using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SystemConnections
{
    public string SystemConnectionId;
    public string ConnectingId;
}

[System.Serializable]
public class SystemData
{
    public SystemConnections[] Connections;
    public string SystemId;
    public string Name;
    public string Type;
    public string NaturalId;
    public float PositionX;
    public float PositionY;
    public float PositionZ;
    public string SectorId;
    public string SubSectorId;
    public string UserNameSubmitted;
    public string Timestamp;
}

[System.Serializable]
public class SystemsData
{
    public SystemData[] systems;
}

[System.Serializable]
public class PlanetData
{
    public string PlanetNaturalId;
    public string PlanetName;
}

[System.Serializable]
public class PlanetsData
{
    public PlanetData[] planets;
}

public class StarSystemsGenerator : MonoBehaviour
{
    [Header("Prefabs")]
    public TextAsset systemsJson;
    public TextAsset planetsJson;
    public GameObject systemObject;
    public GameObject connectionObject;

    [Header("Info")]
    public List<Transform> systems;
    public List<Transform> connectedSystems;
    public Dictionary<Vector3, List<Vector3>> systemConnections = new Dictionary<Vector3, List<Vector3>>();

    private SystemsData GetSystemsData()
    {
        return JsonUtility.FromJson<SystemsData>(systemsJson.text);
    }

    private PlanetsData GetPlanetsData()
    {
        return JsonUtility.FromJson<PlanetsData>(planetsJson.text);
    }

    private void Start()
    {
        SystemsData systems = GetSystemsData();
        PlanetsData planets = GetPlanetsData();
        GenerateStarSystems(systems.systems, planets.planets);
    }

    private void GenerateStarSystems(SystemData[] systemsData, PlanetData[] planetsData)
    {
        foreach (SystemData systemData in systemsData)
        {
            StarSystem starSystem = CreateSystem(systemData);
            AddSystemConnections(systemData, starSystem);
            AddPlanets(starSystem, planetsData);
            systems.Add(starSystem.transform);
        }

        foreach (Transform systemTransform in systems)
        {
            SearchForConnectedSystems(systemTransform);
            connectedSystems.Add(systemTransform);
        }
    }

    private void AddSystemConnections(SystemData systemData, StarSystem systemInfo)
    {
        foreach (SystemConnections connection in systemData.Connections)
        {
            string systemId = connection.SystemConnectionId.Split('-')[1];
            systemInfo.connectedPlanetIds.Add(systemId);
        }
    }

    private void SearchForConnectedSystems(Transform systemTransform)
    {
        StarSystem currentSystemInfo = systemTransform.GetComponent<StarSystem>();
        Vector3 currentSystemPosition = systemTransform.position;
        systemConnections.Add(currentSystemPosition, new List<Vector3>());

        foreach (Transform newSystemTransform in systems)
        {
            string newSystemId = newSystemTransform.GetComponent<StarSystem>().SystemId;
            if (currentSystemInfo.connectedPlanetIds.Contains(newSystemId))
            {
                currentSystemInfo.connections.Add(newSystemTransform);
                systemConnections[currentSystemPosition].Add(newSystemTransform.position);

                if (!connectedSystems.Contains(systemTransform))
                {
                    CreateConnection(currentSystemPosition, newSystemTransform.position);
                }
            }
        }
    }

    private StarSystem CreateSystem(SystemData systemData)
    {
        Vector3 spawnPosition = new Vector3(systemData.PositionX, systemData.PositionY, systemData.PositionZ);
        GameObject system = Instantiate(systemObject, spawnPosition, new Quaternion(0, 0, 0, 0));

        system.name = systemData.Name;
        StarSystem starSystem = system.GetComponent<StarSystem>();
        starSystem.SystemId = systemData.SystemId;
        starSystem.NaturalId = systemData.NaturalId;
        return starSystem;
    }

    private GameObject CreateConnection(Vector3 start, Vector3 end)
    {
        Vector3 midpoint = start + (end - start) / 2;
        GameObject connection = Instantiate(connectionObject, midpoint, new Quaternion(0, 0, 0, 0));
        connection.transform.localScale = new Vector3(1, Vector3.Distance(start, end) / 2, 1);
        connection.transform.up = end - start;
        return connection;
    }

    private void AddPlanets(StarSystem starSystem, PlanetData[] planetsData)
    {
        string systemNaturalId = starSystem.NaturalId;
        foreach (PlanetData planetData in planetsData)
        {
            if (planetData.PlanetNaturalId.Contains(systemNaturalId))
            {
                Planet planet = new Planet();
                planet.InitPlanet(planetData.PlanetName, planetData.PlanetNaturalId);
                starSystem.planets.Add(planet);
            }
        }
    }
}

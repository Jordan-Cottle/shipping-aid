using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSystem : MonoBehaviour
{
    public string SystemId;
    public string NaturalId;
    public List<string> connectedPlanetIds;
    public List<Transform> connections;
    public List<Planet> planets;
}

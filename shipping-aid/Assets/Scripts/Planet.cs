[System.Serializable]
public class Planet
{
    public string Name;
    public string NaturalId;

    public void InitPlanet(string name, string id)
    {
        Name = name;
        NaturalId = id;
    }
}
